import hashlib

from aiogram import Dispatcher, Bot, types

from settings import TG_BOT_TOKEN, BASE_URL
from utils import db_client
from const import PreregSteps
import const

bot = Bot(token=TG_BOT_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands='start')
async def start(message: types.Message):
    answer = const.HELLO_MSG.format(message.from_user.first_name, BASE_URL)
    await message.answer(answer)


@dp.message_handler()
async def registration(message: types.Message):
    user_id = message.from_user.id
    is_finished = db_client.users_col.find_one({'user_id': user_id})
    if is_finished:
        await message.answer(const.FINISHED)
    else:
        prereg = db_client.preregs_col.find_one({'user_id': user_id}) or {}
        step = prereg.get('step', PreregSteps.USERNAME.value)
        if step == PreregSteps.USERNAME.value:
            username = message.text
            already_exist = db_client.users_col.find_one({'username': username})
            if already_exist:
                await message.answer(const.USERNAME_EXISTS)
            else:
                new_prereg = {
                    'user_id': user_id,
                    'username': username,
                    'step': PreregSteps.PASSWORD.value
                }
                db_client.preregs_col.insert_one(new_prereg)
                await message.answer(const.LOGIN_ACCEPTED)
        if step == PreregSteps.PASSWORD.value:
            password = message.text
            if len(password) < 6:
                await message.answer(const.TOO_SHORT)
            else:
                hashed = hashlib.sha1(password.encode()).hexdigest()
                update = {'password': hashed, 'step': PreregSteps.CONFIRM_PASSWORD.value}
                db_client.preregs_col.update_one({'user_id': user_id}, {'$set': update})
                await message.answer(const.PASSWORD_ACCEPTED)
        if step == PreregSteps.CONFIRM_PASSWORD.value:
            confirmed_pass = message.text
            hashed_confirmed_pass = hashlib.sha1(confirmed_pass.encode('utf-8')).hexdigest()
            if hashed_confirmed_pass != prereg.get('password', ''):
                update = {'step': PreregSteps.PASSWORD.value}
                db_client.preregs_col.update_one({'user_id': user_id}, {'$set': update})
                await message.answer(const.UNMATCHED)
            else:
                update = {'step': PreregSteps.FULL_NAME.value}
                db_client.preregs_col.update_one({'user_id': user_id}, {'$set': update})
                await message.answer(const.FULL_NAME)
        if step == PreregSteps.FULL_NAME.value:
            full_name = message.text
            update = {'full_name': full_name, 'step': PreregSteps.PROFILE_PHOTO.value}
            db_client.preregs_col.update_one({'user_id': user_id}, {'$set': update})
            await message.answer(const.PROFILE_PHOTO)


@dp.message_handler(content_types=['photo'])
async def photo_handler(message: types.Message):
    user_id = message.from_user.id
    prereg = db_client.preregs_col.find_one({'user_id': user_id}) or {}
    step = prereg.get('step')
    if step == PreregSteps.PROFILE_PHOTO.value:
        username = prereg.get('username', 'default')
        file_id = message.photo[0].file_id
        photo = await bot.get_file(file_id)
        file_path = photo.file_path
        extension = file_path.split('.')[-1]
        download_path = f'static/photos/{username}.{extension}'
        await bot.download_file(file_path, download_path)
        user = {
            'user_id': user_id,
            'username': prereg['username'],
            'password': prereg['password'],
            'full_name': prereg['full_name'],
            'profile_photo': f'{BASE_URL}/{download_path}'
        }
        db_client.users_col.insert_one(user)
        db_client.preregs_col.delete_one({'user_id': user_id})
        await message.answer(const.END.format(BASE_URL))
