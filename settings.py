import os
from dotenv import load_dotenv

load_dotenv()

TG_BOT_TOKEN = os.getenv("TG_BOT_TOKEN", '')

WEBHOOK_PATH = f"/bot/{TG_BOT_TOKEN}"
WEBHOOK_URL = "https://13.52.247.5" + WEBHOOK_PATH

SSL_PUBLIC_CERT_PATH = os.path.abspath('public.pem')
BASE_URL = os.getenv('base_url', 'https://13.52.247.5')
