import hashlib
from typing import Annotated

import uvicorn
from fastapi import FastAPI, Form, HTTPException
from fastapi.staticfiles import StaticFiles
from aiogram import types, Dispatcher, Bot
from pydantic import BaseModel

from bot import dp, bot
from settings import WEBHOOK_PATH, WEBHOOK_URL, SSL_PUBLIC_CERT_PATH
from utils import db_client


class UserResponse(BaseModel):
    username: str
    full_name: str
    profile_photo: str


app = FastAPI()
app.mount(f"/static", StaticFiles(directory='static'), name='static')


@app.on_event("startup")
async def on_startup():
    await bot.set_webhook(
            url=WEBHOOK_URL,
            certificate=open(SSL_PUBLIC_CERT_PATH, 'rb')
        )
    db_client.init_connection()


@app.get("/health")
async def hello():
    return "Server running"


@app.post("/login")
async def login(username: Annotated[str, Form()], password: Annotated[str, Form()]) -> UserResponse:
    user_data = db_client.users_col.find_one({'username': username})
    if user_data is None:
        raise HTTPException(status_code=404, detail=f"User with username {username} not found")
    received_password = hashlib.sha1(password.encode('utf-8')).hexdigest()
    user_password = user_data.get('password', '')
    if received_password != user_password:
        raise HTTPException(status_code=403, detail="Incorrect password")
    response_dict = UserResponse(
        username=user_data.get('username', ''),
        full_name=user_data.get('full_name', ''),
        profile_photo=user_data.get('profile_photo', ''),
    )
    return response_dict


@app.post(WEBHOOK_PATH)
async def bot_webhook(update: dict):
    telegram_update = types.Update(**update)
    Dispatcher.set_current(dp)
    Bot.set_current(bot)
    await dp.process_update(telegram_update)


@app.on_event("shutdown")
async def on_shutdown():
    await bot.session.close()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
