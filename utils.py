import pymongo


class MongoDB:
    client = None
    db = None
    preregs_col = None
    users_col = None

    def init_connection(self):
        self.client = pymongo.MongoClient("mongodb://localhost:27017/")
        self.db = self.client['bot_db']
        self.preregs_col = self.db['preregs']
        self.users_col = self.db['users']


db_client = MongoDB()
